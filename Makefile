IMAGE := ./image

images: $(IMAGE)
	$(IMAGE) RNBQKBNR position1.png 40
	$(IMAGE) NNBRKQRB position2.png 40
	magick position1.png -crop 400x140+0+260 position1.png
	magick position2.png -crop 400x140+0+260 position2.png

$(IMAGE): image.pas
	fpc -Mobjfpc -Sh $<

article: README.md
	pandoc $< -f gfm -t html -s -o $@.html --css pandoc.css --metadata title="Les échecs aléatoires de Fischer"

demo: fischerandomdemo.pas fischerandom.pas
	fpc -Mobjfpc -Sh $<

alphabet.fen: demo
	./fischerandomdemo

number: number.pas fischerandom.pas
	fpc -Mobjfpc -Sh $<

fischerandom.fen: number
	./number
