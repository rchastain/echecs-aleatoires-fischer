
uses
  SysUtils, Math;

function StartPosition(): string;
  
  type
    TIntegerArray = array of integer;
  
  function EmptySquares(const ARow: string; const IStart, IEnd: integer): TIntegerArray;
  var
    i: integer;
  begin
    SetLength(result, 0);
    for i := IStart to IEnd do
      if ARow[i] = '.' then
      begin
        SetLength(result, Succ(Length(result)));
        result[High(result)] := i;
      end;
  end;
  
  function CastlingRights(const IRook1, IRook2: integer): string;
  begin
    result := Concat(
      Chr(Ord('A') + Pred(IRook1)),
      Chr(Ord('A') + Pred(IRook2)),
      Chr(Ord('a') + Pred(IRook1)),
      Chr(Ord('a') + Pred(IRook2))
    );
  end;
  
var
  i, IRook1, IRook2: integer;
  arr: TIntegerArray;
  blackpieces: string;
begin
  result := StringOfChar('.', 8);
  
  result[2 * Random(4) + 1] := 'B';
  result[2 * Random(4) + 2] := 'B';
  
  arr := EmptySquares(result, 1, 8);
  i := arr[Random(4) + 1];
  result[i] := 'K';
  
  IRook1 := RandomFrom(EmptySquares(result, 1, Pred(i)));
  result[IRook1] := 'R';
  IRook2 := RandomFrom(EmptySquares(result, Succ(i), 8));
  result[IRook2] := 'R';
  
  result[RandomFrom(EmptySquares(result, 1, 8))] := 'Q';
  
  for i := 1 to 8 do
    if result[i] = '.' then
      result[i] := 'N';
  
  blackpieces := LowerCase(result);
  
  result := Concat(blackpieces, '/pppppppp/8/8/8/8/PPPPPPPP/', result, ' w ', CastlingRights(IRook2, IRook1), ' - 0 1');
end;

begin
  Randomize;
  WriteLn(StartPosition());
end.
